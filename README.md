nbstrip_jq
----------

Automates 'cruft' removal from notebooks
Follows Tim Staley's git/jupyter play nice post https://is.gd/YrLIwM

Installation
------------

Place `nbstrip_jq.bat` in a directory that is in your `$PATH`.

Usage
-----

`nbstrip_jq infile [outfile]`

If outfile not specified then infile will be overwritten

Known Issues
------------

If jqv1.5 is installed it is possible that nbstrip_jq will remove **all** text.
In particular, this results in a lost file if nbstrip_jq is called with an
unspecified outfile. Therefore jqv1.6 installation is recommended.
See [here](https://is.gd/d5IYRs "jq Appveyor instructions") for instructions as
to how to do that.