@echo off

rem Automates 'cruft' removal from notebooks
rem Follows Tim Staley's git/jupyter play nice post https://is.gd/YrLIwM
rem Usage: nbstrip_jq infile [outfile]
rem If outfile not specified then infile will be overwritten

if "%~1"=="" goto blank
    set infile=%1
	
if not exist %infile% goto infile_dne
    
set "overwrite="
if "%~2"=="" (
    set "overwrite=y"
    
    rem unique filename for the temp file see https://is.gd/11i7Lf
    for /f "delims=/ tokens=1-3" %%a in ("%DATE:~4%") do (
        for /f "delims=:. tokens=1-4" %%m in ("%TIME: =0%") do (
            set outfile=nbstrip_jq_tmp-%%c-%%b-%%a-%%m%%n%%o%%p
        )
    )
    
    ) else ( 
    set outfile=%2
)

rem cruft removal is done here
@echo Stripping notebook %infile% to %outfile%
@echo on
jq --indent 1 "(.cells[] | select(has(\"outputs\")) | .outputs) = [] | (.cells[] | select(has(\"execution_count\")) | .execution_count) = null | .metadata = {\"language_info\": {\"name\":\"python\", \"pygments_lexer\": \"ipython3\"}} | .cells[].metadata = {}" %infile% > %outfile%
@echo off

if "%overwrite%" == "y" (
    xcopy "%outfile%" "%infile%" /y
    @echo Removing %outfile%
    del "%outfile%"
    )

goto done

:infile_dne
echo nbstrip_jq: %infile% does not exist
goto done

:blank
echo nbstrip_jq: No filename specified

:done
echo nbstrip_jq: completed